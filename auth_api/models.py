from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError

from datetime import timedelta
import random
import string


class AuthToken(models.Model):
    token = models.CharField(max_length=64, primary_key=True)
    service = models.CharField(max_length=1024)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    expire = models.DateTimeField()

    def __str__(self):
        return self.token

    def expired(self):
        if timezone.now() > self.expire:
            return True
        return False

    @staticmethod
    def expiredSet():
        return AuthToken.objects.filter(expire__lt=timezone.now())

    @staticmethod
    def newToken(user, service):
        lastError = None
        for retry in range(3):
            TOKEN_CHARS = string.ascii_letters + string.digits
            TOKEN_SIZE = 32
            token = ''.join(
                [random.choice(TOKEN_CHARS) for _ in range(TOKEN_SIZE)])
            expiracy = timezone.now() + timedelta(days=settings.EXPIRE_DAYS)

            nToken = AuthToken(token=token,
                               service=service,
                               user=user,
                               expire=expiracy)
            try:
                nToken.full_clean()
                nToken.save()
                return nToken
            except ValidationError as e:
                lastError = e
        raise lastError

