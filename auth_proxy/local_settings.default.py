import os

DEBUG = True  # FIXME for prod

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'o%&ve6pt0l%m7eu&k@x3$)1lj77-s!84h#mkr^(&onnhx@zso_'  # FIXME
ALLOWED_HOSTS = []  # FIXME

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
STATIC_URL = '/static/'

# CAS
CAS_SERVER_URL = 'https://cas.example.com/'  # FIXME
CAS_VERSION = '2'  # FIXME if needed

# Tickets TTL
EXPIRE_DAYS = 7  # Number of days a ticket stays valid — FIXME if you want
