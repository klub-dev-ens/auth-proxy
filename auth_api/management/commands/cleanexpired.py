'''
    Cleans the expired tokens
'''

from django.core.management.base import BaseCommand
from auth_api.models import AuthToken


class Command(BaseCommand):
    help = 'Cleans the expired tokens in the database'

    def handle(self, *args, **options):
        expiredSet = AuthToken.expiredSet()
        num = expiredSet.count()
        expiredSet.delete()
        self.stdout.write(self.style.SUCCESS(
            'Cleaned {} expired token{}.'.format(
                num,
                's' if num != 1 else '',
            )))
