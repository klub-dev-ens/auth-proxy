# auth-proxy

Proxy d'authentification pour cas.eleves.ens.fr, gardant des sessions ouvertes
(parce qu'il y a aucune raison que nginx fasse ça tout seul).

Ce site n'expose aucune frontend pour des humains. Il est prévu pour être
appelé par nginx ou tout autre logiciel souhaitant garder des sessions CAS
ouvertes, mais n'ayant pas la possibilité de maintenir ces sessions lui-même
aisément.

## Protocole

### /auth

    /auth?service=<service>

Redirige l'utilisateur·trice pour qu'iel s'authentifie sur le CAS (le passage
est transparent si l'user est déjà connecté·e au CAS). Redirige ensuite sur
`<service>`, en ajoutant un paramètre `token=<token>` à la requête GET.

### /check

    /check?service=<service>&token=<token>

Appelé par le serveur/l'application, et non par le client. Renvoie `200` si le
token est correct, `401` sinon. Le `<service>` doit correspondre au `service`
passé lors de `/auth`.

### /logout

Révoque tous les tickets auprès des différents services, et se déconnecte.

### /whoami

Renvoie `200` et le login de l'utilisateur·trice connecté·e, ou `401` si
l'utilisateur·trice n'est pas connecté·e.

## Commandes additionnelles

### ./manage.py cleanexpired

Nettoie les tickets expirés. En production, il est bon d'appeler cette commande
de temps à autres, avec un systemd timer par exemple.

### ./manage.py django_cas_ng_clean_sessions

Cette application utilise en backend `django-cas-ng`. Comme indiqué dans leur
readme, il est bon de lancer `./manage.py django_cas_ng_clean_sessions` de
temps à autres…

## Installation

Cloner le repo, puis

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt

Configurer l'application :

    cd auth_proxy
    cp local_settings{.default,}.py
    $EDITOR local_settings.py

Éditer **a minima** les `FIXME` :

* `DEBUG` doit être à `False` en production ;
* regénérer une `SECRET_KEY` (avec `pwgen -s 60 1`) ;
* renseigner les `ALLOWED_HOSTS` ;
* (probablement) passer la DB en postgresql ;
* (éventuellement) changer la timezone et la locale ;
* renseigner l'adresse du CAS à utiliser.

Puis,

    cd ..  # Revenir à la racine du projet
    ./manage.py migrate

Le projet est maintenant configuré.
