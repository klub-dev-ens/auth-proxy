from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth import logout as dj_logout
from django.contrib.auth.decorators import login_required
from .models import AuthToken

import urllib.parse as urlparse
from urllib.parse import urlencode


def require_get(requiredArgs):
    ''' Fails if a GET parameter is missing. Adds the given parameter as
    function kwargs. '''

    def decorator(fct):
        def wrap(request, *args, **kwargs):
            if request.method != 'GET':
                return HttpResponse('Only GET requests.', status=405)
            for arg in requiredArgs:
                if arg not in request.GET:
                    return HttpResponse('Missing parameter: {}'.format(arg),
                                        status=400)

                kwargs[arg] = request.GET[arg]

            return fct(request, *args, **kwargs)
        return wrap
    return decorator


def status_200_or_401(fct):
    ''' Catches the return code of `fct`, which should be `True` or `False`.
    Depending on the result, either `200 ok` or `401 bad` is returned. '''
    def wrap(request, *args, **kwargs):
        out = fct(request, *args, **kwargs)
        if out is True:
            return HttpResponse('ok', status=200)
        elif out is False:
            return HttpResponse('bad', status=401)
        return HttpResponse('Server internal error', status=500)
    return wrap


@login_required
@require_get(['service'])
def auth(req, service):
    ''' If the user is not yet logged in, they will get redirected to the login
    page. If not, they'll reach the body of this function and we should issue a
    new token. '''

    token = AuthToken.newToken(req.user, service)
    out_parts = list(urlparse.urlparse(service))
    query = dict(urlparse.parse_qsl(out_parts[4]))
    query.update({'token': token})
    out_parts[4] = urlencode(query)
    out_url = urlparse.urlunparse(out_parts)

    return redirect(out_url)


@require_get(['service', 'token'])
@status_200_or_401
def check(req, service, token):
    if AuthToken.objects.filter(token=token, service=service).count() > 0:
        if AuthToken.objects.filter(token=token, service=service).count() > 1:
            AuthToken.objects.filter(token=token, service=service).delete()
            return False
        else:
            token = AuthToken.objects.get(token=token, service=service)
            if token.expired():
                token.delete()
                return False
            if not token.user.is_authenticated:
                token.delete()
                return False
            return True
    return False


def logout(req):
    AuthToken.objects.filter(user=req.user).delete()
    # ^ If the user is not logged in, the query set is empty -> ok
    dj_logout(req)
    return HttpResponse('Logged out.', status=200)


def whoami(req):
    if req.user.is_authenticated:
        return HttpResponse(req.user.username, status=200)
    return HttpResponse('', status=401)
