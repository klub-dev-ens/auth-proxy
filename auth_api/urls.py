from django.conf.urls import url
from .views import auth, check, logout, whoami

urlpatterns = [
    url(r'^auth', auth, name='auth'),
    url(r'^check', check, name='check'),
    url(r'^logout', logout, name='logout'),
    url(r'^whoami', whoami, name='whoami'),
]
