from django_cas_ng.backends import CASBackend


class CleaningCASBackend(CASBackend):
    def clean_username(self, username):
        return username.lower().strip()
